# Bloc 2 : Projet 5 : algorithmes gloutons (progdyn)
Sac à dos, monnayeur, problèmes, alternatives
### Francois Baldi, Julien Donet, Gilles Grange, Fabrice Missonnier
Nous nous proposerons d'introduire,aux travers d'exemple de problème simples, diverses approches des algorithmes dits "gloutons", et terminerons par les alternatives à ceux-ci les plus courantes.

## Consignes pour accéder à la fiche
- Cloner le projet GitLab avec la commande
<code> git clone https://gitlab.com/missonnier/diu-bloc2-projet </code>
- Ouvrir la fiche avec Jupyther Notebook, disponible notamment sur la plateforme [Anaconda](https://www.anaconda.com/distribution/ "Anaconda ").
